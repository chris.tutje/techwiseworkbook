from flask import Blueprint, request, jsonify
import bcrypt  
from app import app
from pymongo import MongoClient

register_blueprint = Blueprint('register', __name__)

client = MongoClient('localhost', 27017)
db = client['Python-Authentication']
users_collection = db['users']

@register_blueprint.route('/register', methods=['POST'])
def register():
    data = request.get_json()

    hashed_password = bcrypt.hashpw(data['password'].encode('utf-8'), bcrypt.gensalt())

    if users_collection.find_one({'username': data['username']}):
        return jsonify({'message': 'User already exists'}), 400

    user = {
        'username': data['username'],
        'password': hashed_password.decode('utf-8')  
    }
    users_collection.insert_one(user)

    return jsonify({'message': 'User registered successfully'}), 201
