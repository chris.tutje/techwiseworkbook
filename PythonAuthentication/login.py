from flask import Blueprint, request, jsonify
import bcrypt
import jwt
import datetime
from app import app
from pymongo import MongoClient

login_blueprint = Blueprint('login', __name__)

client = MongoClient('localhost', 27017)
db = client['Python-Authentication']
users_collection = db['users']

@login_blueprint.route('/login', methods=['POST'])
def login():
    data = request.get_json()

    user = users_collection.find_one({'username': data['username']})

    if user and bcrypt.checkpw(data['password'].encode('utf-8'), user['password'].encode('utf-8')):
        token = jwt.encode({
            'user': data['username'],
            'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=30)
        }, app.config['SECRET_KEY'])

        return jsonify({'token': token})

    return jsonify({'message': 'Invalid credentials'}), 401
