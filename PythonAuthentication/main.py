from app import app
from login import login_blueprint
from register import register_blueprint
from protected import protected_blueprint

app.register_blueprint(login_blueprint)
app.register_blueprint(register_blueprint)
app.register_blueprint(protected_blueprint)

if __name__ == '__main__':
    app.run(debug=True)
