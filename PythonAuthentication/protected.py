from flask import Blueprint, request, jsonify
import jwt
import datetime
from app import app

protected_blueprint = Blueprint('protected', __name__)


@protected_blueprint.route('/protected', methods=['GET', 'POST'])
def protected():
    if request.method == 'GET':
        token = request.headers.get('Authorization')

        if not token:
            return jsonify({'message': 'Missing token'}), 401

        try:
            decoded_token = jwt.decode(token.split()[1], app.config['SECRET_KEY'], algorithms=["HS256"])
            return jsonify({'message': 'Welcome, {}'.format(decoded_token['user'])})

        except jwt.ExpiredSignatureError:
            return jsonify({'message': 'Token has expired'}), 401

        except jwt.InvalidTokenError:
            return jsonify({'message': 'Invalid token'}), 401

    elif request.method == 'POST':
        data = request.get_json()

        return jsonify({'message': 'Data received successfully'}), 200
